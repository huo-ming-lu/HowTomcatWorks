package ex02.hml.one;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.InetAddress;
import java.net.ServerSocket;
import java.net.Socket;

/**
 * @author milo.huo
 * @date 2023/3/14
 */
public class HttpServer1 {

    // 声明一个结束标识，用来判断是否需要终止服务
    public static boolean shutDown = false;

    public static void main(String[] args) {
        try {
            ServerSocket serverSocket = new ServerSocket(8080, 10, InetAddress.getByName("127.0.0.1"));
            Socket socket;
            while (!shutDown) {
                socket = serverSocket.accept();
                InputStream inputStream = socket.getInputStream();
                Request request = new Request();
                request.parse(inputStream);
                OutputStream outputStream = socket.getOutputStream();
                Response response = new Response(request, outputStream);

                // 不知道为何会有空请求打进来，这里过滤一下
                if(request.getUri() != null) {
                    if (request.getUri().startsWith("/servlet/")) {
                        ServletProcessor1 servletProcessor1 = new ServletProcessor1();
                        servletProcessor1.process(request, response);
                    } else {
                        StaticResourceProcessor staticResourceProcessor = new StaticResourceProcessor();
                        staticResourceProcessor.process(request, response);
                    }
                    // 判断是否是结束服务的请求
                    shutDown = request.getUri().equals("/shutdown");
                }

                inputStream.close();
                socket.close();

            }
        } catch (IOException e) {
            e.printStackTrace();
        }

    }


}
