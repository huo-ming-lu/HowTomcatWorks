package ex02.hml.one;

/**
 * @author milo.huo
 * @date 2023/3/15
 */
public class StaticResourceProcessor {
    public void process(Request request,Response response) {
        response.sendStaticResource();
    }
}
