package ex02.hml.one;

import javax.servlet.Servlet;
import javax.servlet.ServletException;
import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.net.URLClassLoader;

/**
 * 处理servlet请求   /servlet/servletName
 *
 * @author milo.huo
 * @date 2023/3/15
 */
public class ServletProcessor1 {

    public static final String rootDir = System.getProperty("user.dir") + File.separatorChar + "webroot";

    public void process(Request request, Response response) {
        try {
            String uri = request.getUri();
            String servletName = uri.substring(uri.lastIndexOf("/") + 1);

            //首先获取类加载器,这里采用URLClassLoader
            File file = new File(rootDir);
            String repository = (new URL("file", null, file.getCanonicalPath() + File.separator)).toString();
            URL[] urls = new URL[1];
            urls[0] = new URL(null, repository);
            URLClassLoader urlClassLoader = new URLClassLoader(urls);

            //加载servlet对应的类
            Class<?> aClass = urlClassLoader.loadClass(servletName);
            Servlet servlet = (Servlet) aClass.newInstance();

            // 调用servlet的service方法，处理请求并返回结果
            servlet.service(request, response);

        } catch (IOException | ClassNotFoundException | InstantiationException | IllegalAccessException |
                 ServletException e) {
            e.printStackTrace();
        }

    }
}
