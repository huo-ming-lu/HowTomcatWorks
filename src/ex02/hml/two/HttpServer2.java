package ex02.hml.two;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.InetAddress;
import java.net.ServerSocket;
import java.net.Socket;

/**
 * @author milo.huo
 * @date 2023/3/14
 */
public class HttpServer2 {

    public static boolean shutDown = false;

    public static void main(String[] args) {
        try {
            ServerSocket serverSocket = new ServerSocket(8080, 10, InetAddress.getByName("127.0.0.1"));
            Socket socket;
            while (!shutDown) {
                socket = serverSocket.accept();
                InputStream inputStream = socket.getInputStream();
                Request request = new Request();
                request.parse(inputStream);
                OutputStream outputStream = socket.getOutputStream();
                Response response = new Response(request, outputStream);

                // 不知道为何会有空请求打进来，这里过滤一下
                if(request.getUri() != null) {
                    if (request.getUri().startsWith("/servlet/")) {
                        ServletProcessor2 servletProcessor2 = new ServletProcessor2();
                        servletProcessor2.process(request, response);
                    } else {
                        StaticResourceProcessor staticResourceProcessor = new StaticResourceProcessor();
                        staticResourceProcessor.process(request, response);
                    }
                    shutDown = request.getUri().equals("/shutdown");
                }

                inputStream.close();
                socket.close();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

    }


}
