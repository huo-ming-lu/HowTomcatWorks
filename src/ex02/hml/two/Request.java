package ex02.hml.two;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletInputStream;
import javax.servlet.ServletRequest;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.util.Enumeration;
import java.util.Locale;
import java.util.Map;

/**
 * @author milo.huo
 * @date 2023/3/14
 */
public class Request implements ServletRequest {

    private String uri;

    public void parse(InputStream inputStream) {

        try {
            byte[] bytes = new byte[1024];
            int readLenth = inputStream.read(bytes);
            String content = "";
            while (readLenth != -1) {
                content += new String(bytes);
                if (readLenth < 1024) {
                    break;
                }
                readLenth = inputStream.read(bytes);
            }
            System.out.println("request body --->");
            System.out.println(content);
            //获取请求内容中第一个空格和第二个空格之间的内容
            setUri(content);

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void setUri(String content) {
        int index1 = content.indexOf(" ");
        if (index1 == -1) {
            return;
        }
        int index2 = content.indexOf(" ", index1 + 1);
        String substring = content.substring(index1 + 1, index2);
        this.uri = substring;
    }

    public String getUri() {
        return uri;
    }

    @Override
    public Object getAttribute(String s) {
        return null;
    }

    @Override
    public Enumeration getAttributeNames() {
        return null;
    }

    @Override
    public String getCharacterEncoding() {
        return null;
    }

    @Override
    public void setCharacterEncoding(String s) throws UnsupportedEncodingException {

    }

    @Override
    public int getContentLength() {
        return 0;
    }

    @Override
    public String getContentType() {
        return null;
    }

    @Override
    public ServletInputStream getInputStream() throws IOException {
        return null;
    }

    @Override
    public String getParameter(String s) {
        return null;
    }

    @Override
    public Enumeration getParameterNames() {
        return null;
    }

    @Override
    public String[] getParameterValues(String s) {
        return new String[0];
    }

    @Override
    public Map getParameterMap() {
        return null;
    }

    @Override
    public String getProtocol() {
        return null;
    }

    @Override
    public String getScheme() {
        return null;
    }

    @Override
    public String getServerName() {
        return null;
    }

    @Override
    public int getServerPort() {
        return 0;
    }

    @Override
    public BufferedReader getReader() throws IOException {
        return null;
    }

    @Override
    public String getRemoteAddr() {
        return null;
    }

    @Override
    public String getRemoteHost() {
        return null;
    }

    @Override
    public void setAttribute(String s, Object o) {

    }

    @Override
    public void removeAttribute(String s) {

    }

    @Override
    public Locale getLocale() {
        return null;
    }

    @Override
    public Enumeration getLocales() {
        return null;
    }

    @Override
    public boolean isSecure() {
        return false;
    }

    @Override
    public RequestDispatcher getRequestDispatcher(String s) {
        return null;
    }

    @Override
    public String getRealPath(String s) {
        return null;
    }
}
