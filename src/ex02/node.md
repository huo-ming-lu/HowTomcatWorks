##### 输出流
输出流 out.println(aaa) 相当于 out.print(aaa+"\r\n")

##### HTTP返回格式
Http相应的header中，两种格式表示内容长度
Content-Length:3   (计算出返回body的长度，放到header里)
Transfer-Encoding: chunked  (返回body分块返回，每块内容单独计算长度【16进制的长度值】)

示例：
下面示例中的换行放到代码中皆为 \r\n
```
HTTP/1.1 200 OK
Content-Type: text/html
Content-Length: 32

<h1>server already shutdown</h1>
```

```
HTTP/1.1 200 OK
Content-Type: text/plain
Transfer-Encoding: chunked

23
This is the data in the first chunk
c
second chunk
0\r\n\r\n
```
chunkedg格式的返回内容，客户端会将他们紧密的拼接到一起

##### 编译相关javac
```
javac -cp "../lib/*" PrimitiveServlet.java
javac -classpath "../lib/*" PrimitiveServlet.java
```
使用 -cp 与 -classpath效果一样  后面参数值要带双引号,否则仍找不到jar包
