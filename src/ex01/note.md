本章节将构建一个简单的http服务

功能：接收浏览器的访问请求 /index.html，将静态资源index.html返回给客户端
    接收到 /shutdown请求时，停止后台服务

主要知识点：
ServerSocket 用来构建服务端socket
Socket 用来构建客户端socket，和服务端用来和客户端通信的socket

Response中Content-Length的计算，可以使用fileInputStream.available()来获取文件流的总长度当做返回长度。


HTTP请求内容示例
GET /index.html HTTP/1.1
Host: 127.0.0.1:8080
Connection: keep-alive
Cache-Control: max-age=0
sec-ch-ua: "Google Chrome";v="111", "Not(A:Brand";v="8", "Chromium";v="111"
sec-ch-ua-mobile: ?0
sec-ch-ua-platform: "macOS"
Upgrade-Insecure-Requests: 1
User-Agent: Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/111.0.0.0 Safari/537.36
Accept: text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.7
Sec-Fetch-Site: none
Sec-Fetch-Mode: navigate
Sec-Fetch-User: ?1
Sec-Fetch-Dest: document
Accept-Encoding: gzip, deflate, br
Accept-Language: zh-CN,zh;q=0.9

                                                                                                                                                                                                                                                                                                                                       

HTTP返回内容示例
HTTP/1.1 200 OK
Content-Type: text/html
Content-Length: 32

<h1>server already shutdown</h1>

Http返回静态资源时，需要以字节流的形式返回，不能使用字符串。
返回内容中的Content-Type 可以根据实际的文件类型，选择不同的值。

