package ex01.hml;

import java.io.IOException;
import java.io.InputStream;

/**
 * @author milo.huo
 * @date 2023/3/14
 */
public class Request {

    private String uri;

    // 从输入流中解析出 HTTP 协议内容，并得到客户端要请求的资源uri
    public void parse(InputStream inputStream) {

        try {
            byte[] bytes = new byte[1024];
            int readLenth = inputStream.read(bytes);
            String content = "";
            while(readLenth != -1) {
                content  += new String(bytes);
                if(readLenth < 1024) {
                    break;
                }
                readLenth = inputStream.read(bytes);
            }
            System.out.println("request body --->");
            System.out.println(content);
            //获取请求头中第一个空格和第二个空格之间的内容,例如：请求头 【GET /index.html HTTP/1.1】，uri即为 【/index.html】
            setUri(content);

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void setUri(String content) {
        int index1 = content.indexOf(" ");
        if(index1 == -1) {
            return;
        }
        int index2 = content.indexOf(" ", index1+1);
        String substring = content.substring(index1+1, index2);
        this.uri = substring;
    }

    public String getUri() {
        return uri;
    }

}
