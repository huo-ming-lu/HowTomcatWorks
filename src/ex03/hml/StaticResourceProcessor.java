package ex03.hml;

import ex03.hml.connector.http.HttpRequest;
import ex03.hml.connector.http.HttpResponse;

public class StaticResourceProcessor {

    public void process(HttpRequest request, HttpResponse response) {
        response.sendStaticResource();
    }

}
