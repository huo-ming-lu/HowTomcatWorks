package ex03.hml;

import ex03.hml.connector.http.*;

import javax.servlet.Servlet;
import javax.servlet.ServletException;
import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.net.URLClassLoader;

public class ServletProcessor {

    public void process(HttpRequest request, HttpResponse response) {
        try {
            String uri = request.getRequestURI();
            String servletName = uri.substring(uri.lastIndexOf("/") + 1);

            //首先获取类加载器
            File file = new File(Constants.WEB_ROOT);
            String repository = (new URL("file", null, file.getCanonicalPath() + File.separator)).toString();
            URL[] urls = new URL[1];
            urls[0] = new URL(null, repository);
            URLClassLoader urlClassLoader = new URLClassLoader(urls);

            //加载servlet对应的类
            Class<?> aClass = urlClassLoader.loadClass(servletName);
            Servlet servlet = (Servlet) aClass.newInstance();

            HttpRequestFacade requestFacade = new HttpRequestFacade(request);
            HttpResponseFacade responseFacade = new HttpResponseFacade(response);

            servlet.service(requestFacade, responseFacade);
            response.finishResponse();

        } catch (IOException | ClassNotFoundException | InstantiationException | IllegalAccessException |
                 ServletException e) {
            e.printStackTrace();
        }

    }

}