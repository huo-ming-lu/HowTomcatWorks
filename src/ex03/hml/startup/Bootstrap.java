package ex03.hml.startup;

import ex03.hml.connector.http.HttpConnector;

/**
 * 启动器，用于启动一个Web应用
 */
public final class Bootstrap {
  public static void main(String[] args) {
    HttpConnector connector = new HttpConnector();
    connector.start();
  }
}