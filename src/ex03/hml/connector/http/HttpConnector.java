package ex03.hml.connector.http;

import java.io.IOException;
import java.net.InetAddress;
import java.net.ServerSocket;
import java.net.Socket;

/**
 * 连接器，用于接收socket连接（一次http请求建立一次连接，http返回后销毁连接）
 * 此连接器是以一个独立线程的方式启动起来的
 */
public class HttpConnector implements Runnable {

    boolean stopped;

    // scheme这个属性在本章暂时没地方用到
    private String scheme = "http";

    public String getScheme() {
        return scheme;
    }

    public void run() {
        // 创建一个ServerSocket用来接收客户端的Socket连接
        ServerSocket serverSocket = null;
        int port = 8080;
        try {
            serverSocket = new ServerSocket(port, 1, InetAddress.getByName("127.0.0.1"));
        } catch (IOException e) {
            e.printStackTrace();
            System.exit(1);
        }
        // 建立循环，不停的等待并处理socket连接，这里虽然设了停止标记（stopped），但是暂时没用到，停止服务仍然采用终止进程的方式，
        // 如何优雅的停止服务在后面的章节会有设计
        while (!stopped) {
            Socket socket;
            try {
                // 阻塞等待下一个 Socket 连接
                socket = serverSocket.accept();
            } catch (Exception e) {
                continue;
            }
            // 新建一个HttpProcessor来处理此 Socket 请求
            HttpProcessor processor = new HttpProcessor(this);
            processor.process(socket);
        }
    }

    /**
     * 启动连接器的线程
     */
    public void start() {
        Thread thread = new Thread(this);
        thread.start();
    }
}