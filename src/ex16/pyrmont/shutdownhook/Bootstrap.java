package ex16.pyrmont.shutdownhook;

import ex14.pyrmont.core.SimpleContextConfig;
import org.apache.catalina.*;
import org.apache.catalina.connector.http.HttpConnector;
import org.apache.catalina.core.*;
import org.apache.catalina.loader.WebappLoader;

public final class Bootstrap {

    public static Thread myShutDownHook;
    public static void main(String[] args) {

        System.setProperty("catalina.base", System.getProperty("user.dir"));
        Connector connector = new HttpConnector();

        Wrapper wrapper1 = new StandardWrapper();
        wrapper1.setName("Primitive");
        wrapper1.setServletClass("PrimitiveServlet");
        Wrapper wrapper2 = new StandardWrapper();
        wrapper2.setName("Modern");
        wrapper2.setServletClass("ModernServlet");

        Context context = new StandardContext();
        // StandardContext's start method adds a default mapper
        context.setPath("/app1");
        context.setDocBase("app1");

        context.addChild(wrapper1);
        context.addChild(wrapper2);

        LifecycleListener listener = new SimpleContextConfig();
        ((Lifecycle) context).addLifecycleListener(listener);

        Host host = new StandardHost();
        host.addChild(context);
        host.setName("localhost");
        host.setAppBase("webapps");

        Loader loader = new WebappLoader();
        context.setLoader(loader);
        // context.addServletMapping(pattern, name);
        context.addServletMapping("/Primitive", "Primitive");
        context.addServletMapping("/Modern", "Modern");

        Engine engine = new StandardEngine();
        engine.addChild(host);
        engine.setDefaultHost("localhost");

        Service service = new StandardService();
        service.setName("Stand-alone Service");
        Server server = new StandardServer();
        server.addService(service);
        service.addConnector(connector);

        //StandardService class's setContainer will call all its connector's setContainer method
        service.setContainer(engine);

        // Start the new server

        try {
            // 1.初始化并启动Tomcat
            server.initialize();
            ((Lifecycle) server).start();

            myShutDownHook = new MyShutDownHook(server);
            Runtime.getRuntime().addShutdownHook(myShutDownHook);

            // 2.阻塞等待，直到收到"关闭Tomcat"的消息
            server.await();
        } catch (LifecycleException e) {
            e.printStackTrace(System.out);
        }

        // 3.关闭Tomcat服务
        try {
            String name = java.lang.Thread.currentThread().getName();
            System.out.println("====线程["+name+"]====关闭方式1====Bootstrap即将执行server#stop方法");
            ((Lifecycle) server).stop();
        } catch (LifecycleException e) {
            e.printStackTrace(System.out);
        }

    }


    private static class MyShutDownHook extends Thread {

        private Server server;

        public MyShutDownHook(Server server) {
            this.server = server;
        }

        @Override
        public void run() {
            String name = java.lang.Thread.currentThread().getName();
            System.out.println("====线程["+name+"]====关闭方式2====MyShutDownHook即将执行server#stop方法");
            try {
                ((Lifecycle) server).stop();
            } catch (LifecycleException e) {
                throw new RuntimeException(e);
            }
        }
    }

}