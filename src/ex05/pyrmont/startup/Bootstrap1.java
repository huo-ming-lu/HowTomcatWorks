package ex05.pyrmont.startup;

import ex05.pyrmont.core.SimpleLoader;
import ex05.pyrmont.core.SimpleWrapper;
import ex05.pyrmont.valves.ClientIPLoggerValve;
import ex05.pyrmont.valves.HeaderLoggerValve;
import org.apache.catalina.Loader;
import org.apache.catalina.Pipeline;
import org.apache.catalina.Valve;
import org.apache.catalina.Wrapper;
import org.apache.catalina.connector.http.HttpConnector;

public final class Bootstrap1 {
    public static void main(String[] args) {

        /* call by using http://localhost:8080/ModernServlet,
           but could be invoked by any name */

        // 使用Tomcat的默认连接器
        HttpConnector connector = new HttpConnector();

        // 构建Wrapper容器
        Wrapper wrapper = new SimpleWrapper();
        wrapper.setServletClass("ModernServlet");
        Loader loader = new SimpleLoader();
        Valve valve1 = new HeaderLoggerValve();
        Valve valve2 = new ClientIPLoggerValve();

        wrapper.setLoader(loader);
        ((Pipeline) wrapper).addValve(valve1);
        ((Pipeline) wrapper).addValve(valve2);

        // 将wrapper容器与连接器做个关联
        connector.setContainer(wrapper);

        try {
            connector.initialize();
            connector.start();

            // 保活main线程，直到键盘输入了任何字符
            System.in.read();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}