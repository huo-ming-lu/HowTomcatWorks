package ex05.pyrmont.core;

import org.apache.catalina.*;

import javax.servlet.http.HttpServletRequest;

public class SimpleContextMapper implements Mapper {

    // 映射器关联的Context容器对象
    private SimpleContext context = null;

    public Container getContainer() {
        return (context);
    }

    public void setContainer(Container container) {
        if (!(container instanceof SimpleContext)) {
            throw new IllegalArgumentException("Illegal type of container");
        }
        context = (SimpleContext) container;
    }

    /**
     * 根据请求的特征返回子容器，该子容器用于处理此请求。
     * 如果没有这样的子容器可以被识别，则返回null
     */
    public Container map(Request request, boolean update) {
        // 获取相对路径的uri作为映射的key
        String contextPath = ((HttpServletRequest) request.getRequest()).getContextPath();
        String requestURI = ((HttpRequest) request).getDecodedRequestURI();
        String relativeURI = requestURI.substring(contextPath.length());
        // 调用Context中相关方法拿到特定Wrapper
        Wrapper wrapper = null;
        String servletPath = relativeURI;
        String name = context.findServletMapping(servletPath);
        if (name != null) {
            wrapper = (Wrapper) context.findChild(name);
        }
        return (wrapper);
    }

    public String getProtocol() {
        return null;
    }

    public void setProtocol(String protocol) {
    }

}