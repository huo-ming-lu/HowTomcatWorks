/* explains Tomcat's default container */
package ex04.pyrmont.startup;

import ex04.pyrmont.core.SimpleContainer;
import org.apache.catalina.connector.http.HttpConnector;

public final class Bootstrap {
    public static void main(String[] args) {
        // 创建连接器与Servlet容器，并做一个关联
        HttpConnector connector = new HttpConnector();
        SimpleContainer container = new SimpleContainer();
        connector.setContainer(container);
        try {
            connector.initialize();
            connector.start();

            // 接收键盘输入，在这里的目的是保活main线程
            System.in.read();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}