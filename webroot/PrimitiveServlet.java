import javax.servlet.*;
import java.io.IOException;
import java.io.PrintWriter;

/**
 * http返回格式为
 * Transfer-Encoding: chunked
 * 时，返回格式如下
 * <p>
 * HTTP/1.1 200 OK
 * Content-Type: text/plain
 * Transfer-Encoding: chunked
 * \r\n
 * 16进制的内容1长度\r\n
 * 内容1\r\n
 * 内容2长度\r\n
 * 内容2\r\n
 * 0\r\n\r\n (结尾必须是0和空内容，组合起来就是 0\r\n\r\n)
 */
public class PrimitiveServlet implements Servlet {

    public void init(ServletConfig config) throws ServletException {
        System.out.println("PrimitiveServlet -- init");
    }

    public void service(ServletRequest request, ServletResponse response)
            throws ServletException, IOException {
        PrintWriter out = response.getWriter();

        String body = "This is the response data";
        String header = "HTTP/1.1 200 OK\r\n" +
                "Content-Type: text/html\r\n" +
                "Content-Length: " + body.length() + "\r\n" +
                "\r\n";

        out.print(header);
        out.print(body);

        out.flush();
        out.close();
    }

    public void service_chunked(ServletRequest request, ServletResponse response)
            throws ServletException, IOException {
        PrintWriter out = response.getWriter();

        String msg = "HTTP/1.1 200 OK\r\n" +
                "Content-Type: text/plain\r\n" +
                "Transfer-Encoding: chunked\r\n" +
                "\r\n";
        out.print(msg);

        String msg1 = "This is the data in the first chunk\r\n";
        out.print(Integer.toHexString(msg1.length()) + "\r\n");
        out.print(msg1 + "\r\n");

        String msg2 = "This is the data in the second chunk\r\n";
        out.print(Integer.toHexString(msg2.length()) + "\r\n");
        out.print(msg2 + "\r\n");

        out.print("0\r\n\r\n");
        out.flush();
        out.close();
    }

    public void destroy() {
        System.out.println("PrimitiveServlet -- destroy");
    }

    public String getServletInfo() {
        return null;
    }

    public ServletConfig getServletConfig() {
        return null;
    }

}
