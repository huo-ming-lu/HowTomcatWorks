import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Enumeration;

public class ModernServlet extends HttpServlet {

    public void init(ServletConfig config) {
        System.out.println("ModernServlet -- init");
    }

    public void doGet(HttpServletRequest request,
                      HttpServletResponse response)
            throws ServletException, IOException {

        response.setContentType("text/html");
        PrintWriter out = response.getWriter();
        //先输出HTTP的头部信息
        String msg = "HTTP/1.1 200 OK\r\n" +
                "Content-Type: text/html\r\n" +
                "Transfer-Encoding: chunked\r\n" +
                "\r\n";
        out.print(msg);

        StringBuilder builder = new StringBuilder();
        //再输出HTTP的消息体
        builder.append("<html>");
        builder.append("<head>");
        builder.append("<title>Modern Servlet</title>");
        builder.append("</head>");
        builder.append("<body>");

        builder.append("<h2>Headers</h2>");
        Enumeration headers = request.getHeaderNames();
        while (headers.hasMoreElements()) {
            String header = (String) headers.nextElement();
            builder.append("<br>" + header + " : " + request.getHeader(header));
        }

        builder.append("<br><h2>Method</h2>");
        builder.append("<br>" + request.getMethod());

        builder.append("<br><h2>Parameters</h2>");
        Enumeration parameters = request.getParameterNames();
        while (parameters.hasMoreElements()) {
            String parameter = (String) parameters.nextElement();
            builder.append("<br>" + parameter + " : " + request.getParameter(parameter));
        }

        builder.append("<br><h2>Query String</h2>");
        builder.append("<br>" + request.getQueryString());

        builder.append("<br><h2>Request URI</h2>");
        builder.append("<br>" + request.getRequestURI());

        builder.append("</body>");
        builder.append("</html>");

        // 这里是与原书中代码不一样的地方，原代码没有加chunked块的长度，浏览器不能正常解析
        out.print(Integer.toHexString(builder.length()) + "\r\n");
        out.print(builder.toString() + "\r\n");

        out.print("0\r\n\r\n");
        out.flush();
        out.close();
    }
}